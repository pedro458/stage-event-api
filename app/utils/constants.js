/**
 * Opções do mongoose para evitar deprecation warning
 */
const MONGOOSE_OPTS = {
    useFindAndModify  : false,
    useCreateIndex    : true,
    useNewUrlParser   : true,
};

/* Opções para o campo status */
const REQUEST_EVENT_STATUS = {
    PENDING: 'pendente',
    ACCEPTED: 'aceita',
    DECLINED: 'recusada'
}

module.exports = {
    REQUEST_EVENT_STATUS,
    MONGOOSE_OPTS
}