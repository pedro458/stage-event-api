const mongoose = require('mongoose');

const pointSchema = new mongoose.Schema({
    type: {
        type: String,
        enum: ['Point'],
        required: true
    },
    coordinates: {
        type: [Number],
        required: true
    }
}, { _id: false });


/**
 * Faz a paginação dos resultados segundo uma quantidade de resultados por
 * página e o número da página. Considera que a numeraçãoo inicia no 0.
 * @param {Number} page Numero da página.
 * @param {Number} perPage Quantidade de resultados por página.
 */
function paginatePlugin(schema) {
    schema.query.paginate = function(page, perPage) { 
      return this.skip(page * perPage).limit(perPage);
    }
  }


module.exports = {
    pointSchema,
    paginatePlugin
}