const Joi = require('joi');

const basicString = Joi.string().min(1).strict();
const emailSchema = Joi.string().email();
const passwordSchema = Joi.string().strict().min(6);
const fullNameSchema = Joi.string().regex(/^[\w\W]+( [\w\W]+)+$/).max(50);
const phoneSchema = Joi.string().regex(/^[0-9]{11}$/).strict();
const idSchema = Joi.string().regex(/^[0-9a-fA-F]{24}$/).strict();
const acronSchema = Joi.string().regex(/^[\w]{0,8}$/).strict();
const urlSchema = Joi.string().uri();
const dateSchema = Joi.date().iso();
const arrayOfIdsSchema = Joi.array().items(idSchema.required());

/** Schemas para o campo de location */
const longSchema = Joi.number().min(-180).max(180);
const latSchema = Joi.number().min(-90).max(90);
const coordinatesSchema = Joi.array().items([
  longSchema.required(), latSchema.required()
]);
const locationSchema = Joi.object().keys({
  type: 'Point',
  coordinates: coordinatesSchema,
});
const fullAdressSchema = Joi.object().keys({
    location: locationSchema.required(),
    state: basicString.required(),
    city: basicString.required(),
    address: basicString.required()
})

module.exports = {
    emailSchema,
    passwordSchema,
    fullNameSchema,
    phoneSchema,
    idSchema,
    acronSchema,
    urlSchema,
    dateSchema,
    arrayOfIdsSchema,
    fullAdressSchema
}