const Joi = require('joi');
const { 
    fullNameSchema,
    acronSchema,
    idSchema,
    emailSchema,
    urlSchema,
    dateSchema,
    arrayOfIdsSchema,
    fullAdressSchema
 } = require('./baseSchemas');

/** 
 * Schema para validar o endpoint de requisitar uma nova solitação
*/
const requestSchema = Joi.object().keys({
    body: {
        acron: acronSchema.required(),
        title: fullNameSchema.required(),
        description: Joi.string().required(),
        email: emailSchema.required(),
        photo_logo: urlSchema.required(),
        begin_date: dateSchema.required(),
        end_date: dateSchema.required(),
        managers: arrayOfIdsSchema,
        organization: idSchema.required(),
        location: fullAdressSchema.required()
    }
})

module.exports = {
    requestSchema
}