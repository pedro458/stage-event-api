const eventSchemas = require('./eventSchemas');

module.exports = {
    'POST /events/request': eventSchemas.requestSchema
}