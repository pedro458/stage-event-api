'use strict';

const utils = require('util');
const exec = utils.promisify(require('child_process').exec);
const hygen = require('hygen');

const generateNewApp = async (params) => {
    const { 
        eventName,
        path
    } = params;

    // Removendo espaços em branco no nome do evento
    const eventNameTrim = eventName.replace(/\s/g, '\\ ');

    const { stdout, stderr } = await Promise.all(
        exec(`hygen event config --root_path ${path} --event_name ${eventNameTrim}`),
        exec(`hygen event api --root_path ${path} --event_name ${eventNameTrim}`),
        exec(`hygen event components --root_path ${path} --event_name ${eventNameTrim}`),
        exec(`hygen event assets --root_path ${path} --event_name ${eventNameTrim}`),
        exec(`hygen event pages --root_path ${path} --event_name ${eventNameTrim}`),
    )
    return stdout;
}

module.exports = {
    generateNewApp
}