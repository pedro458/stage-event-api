'use strict';

const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const cors = require('@koa/cors');
const logging = require('@kasa/koa-logging');
const requestId = require('@kasa/koa-request-id');
const apmMiddleware = require('./middlewares/apm');
const errorHandler = require('koa-better-error-handler');
const koa404Handler = require('koa-404-handler');
const responseHandler = require('./middlewares/responseHandler');
const logger = require('./logger');
const router = require('./routes');
const mongoose = require('mongoose');
const eventsRouter = require('./routes/eventsRoutes');
const { setTennantOnChain } = require('./middlewares/tennantMiddleware');
const ClsAdapter = require('@emartech/cls-adapter');

class App extends Koa {
  constructor(...params) {
    super(...params);
    // Trust proxy
    this.proxy = true;
    // Disable `console.errors` except development env
    this.silent = this.env !== 'development';

    this.servers = [];

    this._configureEnv();
    this._configureMiddlewares();
    this._configureRoutes();
  }

  _configureMiddlewares() {
    this.use(
      bodyParser({
        enableTypes: ['json', 'form'],
        formLimit: '10mb',
        jsonLimit: '10mb'
      })
    );
    this.use(requestId());
    this.use(
      cors({
        origin: '*',
        allowMethods: ['GET', 'HEAD', 'PUT', 'POST', 'DELETE', 'PATCH'],
        allowHeaders: ['Content-Type', 'Authorization'],
        exposeHeaders: ['Content-Length', 'Date', 'X-Request-Id']
      })
    );
    // Putting tennantId on middleware
    this.use(ClsAdapter.getKoaMiddleware());
    this.use(setTennantOnChain);
    this.use(responseHandler());
    this.use(apmMiddleware());
    this.use(logging({
      logger,
      overrideSerializers: false
    }));
    // Error handler
    this.context.onerror = errorHandler;
    this.context.api = true;
    this.use(koa404Handler);
  }

  _configureRoutes() {
    // Bootstrap application router
    this.use(router.routes());
    this.use(router.allowedMethods());
    this.use(eventsRouter.routes());
  }

  _configureDb(){
    mongoose.plugin(require('./utils/utils').paginatePlugin);
  }

  _configureEnv(){
    if(process.env.NODE_ENV !== 'production') {
      require('dotenv').load();
    }
  }

  listen(...args) {
    const server = super.listen(...args);
    this.servers.push(server);
    return server;
  }

  async terminate() {
    // TODO: Implement graceful shutdown with pending request counter
    for (const server of this.servers) {
      server.close();
    }
  }
}

module.exports = App;
