const ClsAdapter = require('@emartech/cls-adapter');
const Tennant = require('../models/Tennant');

exports.setTennantOnChain = async (ctx, next) => {
    let tenant = null;
    try {
        const Model = await Tennant;
        tenant = await Model.findOne({ host: ctx.request.origin });
    } catch(error) {
        console.log('Error >> ', error);
    }

    if(tenant !== null) {
        ClsAdapter.setOnContext('tenantId', tenant.tennantId);
    }

    await next();
}