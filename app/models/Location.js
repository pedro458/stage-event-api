'use strict';

const mongoose = require('mongoose');
const { pointSchema } = require('../utils/utils');

const LocationSchema = new mongoose.Schema({
    address: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true,
    },
    state: {
        type: String,
        required: true
    },
    location: {
        type: pointSchema,
        required: true
    }
}, { timestamps: true })

module.exports = LocationSchema