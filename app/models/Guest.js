'use strict';

const mongoose = require('mongoose');
require('mongoose-type-email');
require('./Activity');

const { tenantModel } = require('../modules/multiTennant');

const GuestSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: mongoose.SchemaTypes.Email,
        required: false
    },
    bio: {
        type: String,
        required: true,
    },
    photo: {
        type: String,
        required: true,
    },
    lattes: {
        type: String,
        required: false
    },
    twitter: {
        type: String,
        required: false
    },
    instagram: {
        type: String,
        required: false
    },
    facebook: {
        type: String,
        required: false
    },
    showOnPage: {
        type: Boolean,
        required: false
    },
    activity: {
        type: mongoose.Schema.Types.ObjectId,
        ref: Activity
    }
}, { timestamps: true });

const Guest = tenantModel('Guest', GuestSchema);

module.exports = Guest;