'use strict';

const mongoose = require('mongoose');
require('mongoose-type-email');
require('mongoose-uuid2')(mongoose);

const { tenantModel } = require('../modules/multiTennant');
const locationSchema = require('./Location')

const EventSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    email: {
        type: mongoose.SchemaTypes.Email,
        required: true,
        unique: true
    },
    acron: {
        type: String,
        required: true,
        maxlength: 8
    },
    photo_logo: {
        type: String,
    },
    begin_date: {
        type: Date,
        required: true
    },
    end_date: {
        type: Date,
        required: true
    },
    enabled: {
        type: Boolean,
        required: true,
        default: false
    },
    location: locationSchema,
}, { timestamps: true })

const Event = tenantModel('Event', EventSchema);

module.exports = Event;