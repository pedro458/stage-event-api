'use strict';

const mongoose = require('mongoose');
require('mongoose-uuid2')(mongoose);

const { tenantlessModel } = require('../modules/multiTennant');

const TennantSchema = new mongoose.Schema({
    tennantId: {
        type: mongoose.Types.UUID,
        required: true
    },
    host: {
        type: String,
        required: true,
        unique: true
    }   
}, { timestamps: true });

const Tennant = tenantlessModel('Tennant', TennantSchema);

module.exports = Tennant;