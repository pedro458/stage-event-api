const Joi = require('joi');
const err = require('../constants/error');
const schemas = require('../utils/schemas/schemasRouter');

// Opções de validação
const _validationOptions = {
    abortEarly: false, // Abortar após o ultimo erro de validação
    allowUnknown: true, // Permitir que chaves desconhecidas serão ignoradas
    stripUnknown: true // Remover chaves desconhecidas de dados validados
};

/**
 * Valida um objeto de acordo com um schema e opções, utilizando o Joi.
 * @param {Object} data Dados a serem validados.
 * @param {Object} schema Schema que valida os dados.
 * @param {Object} options Opções de validação.
 */
function validateData(data, schema, options = _validationOptions) {
    return new Promise((resolve, reject) => {
      const result = Joi.validate(data, schema, options);
      result.error ? reject(result.error) : resolve(result.value);
    });
  }

/**
 * Middleware de validação de requisições.
 * @param {*} ctx Contexto da request
 * @param {*} next Callback da proxima função na pilha de execução
 */
exports.validateRequest = async function (ctx, next) {
    try {
      // Obter caminho da requisição
      const path = ctx.req.url
      
      // Obter método
      const method = ctx.req.method;
  
      // Montar nome do esquema
      const name = `${ method } ${ path }`;
      
      // Obter schema correspondente ao nome
      const schema = schemas[name];
          
      // Criar objeto que será validado
      const data = { 
        body: ctx.request.body,
        params: ctx.request.params,
        query: ctx.request.query
      }
  
      // Validar objeto com o schema correspondente
      const validData = await validateData(data, schema);

      // Atualizar a requisição com os dados validados
      [ ctx.request.body = ctx.request.body, ctx.request.params = ctx.request.params, ctx.request.query = ctx.request.query ] 
      = [ validData.body, validData.params, validData.query ]

      return next()
    } catch (error) {
      ctx.throw(err.VALIDATION_ERROR.statusCode, error.message)
    }
}
    