const mongoose = require('mongoose');
const { Schema } = require('mongoose');
const ClsAdapter = require('@emartech/cls-adapter');

exports.tenantModel = async (name, schema, options) => {
    return(props = {}) => {
        schema.add({ tenantId: String });
        const Model = mongoose.model(name, schema, options);

        const { skipTenant, tenantId } = props;
        if(skipTenant) return Model;

        const context = ClsAdapter.getContextStorage();
        let tenant = context['tenantId'];
        if(tenantId){
            tenant = tenantId;
        }

        Model.schema.set('discriminatorKey', 'tenantId');
        const discriminatorName = `${Model.modelName}-${tenant}`;
        const existingDiscriminator = (Model.discriminators || {})[discriminatorName];
        return existingDiscriminator || Model.discriminator(discriminatorName, new Schema({}));
    };
}

exports.tenantlessModel = async (name, schema, options) => {
    return new Promise((resolve, reject) =>
             resolve(mongoose.model(name, schema, options))
        );
}