'use strict';

const uuidv4 = require('uuid/v4');

const {
    generateNewApp
} = require('../services');

const {
    EMAIL_CONFLICT
} = require('../constants/error');

const EventModel = require('../models/Event');
const TenantModel = require('../models/Tennant');

/**
 * Aprovar uma requisição interagindo com a API de dados do evento
 * @param {*} ctx 
 */
exports.approveRequest = async ctx => {

    // Obter os dados dos eventos
    const { ...eventData } = ctx.request.body;    

    // Instanciando os models
    const Event = await EventModel;
    const Tenant = await TenantModel;

    // Checando para ver se já tem o mesmo evento cadastrado
    const eventConflict = await Event({ skipTenant: true }).findOne({ email: eventData.email })
    
    if(eventConflict){
        ctx.throw(EMAIL_CONFLICT.statusCode, EMAIL_CONFLICT.message)
    }

    // Gerando o tennant id
    const tennantId = uuidv4();

    // Criando o tennant
    const tennant = await Tenant.create({ tennantId, host:
         eventData.host });

    // Chamando o service que gera os arquivos
    const params = {
        eventName: eventData.title,
        path: `../../${eventData.acron}`
    }
    const stdout = generateNewApp(params);

    // Criando o evento no banco
    const event = await Event({ tenantId: tennant.tennantId }).create({ ...eventData });

    ctx.body = {
        title: event.title,
        message: 'evento registrado com sucesso'
    }
}

exports.getBasicInfo = async ctx => {
    // Instanciando o model
    const Event = await EventModel;

    let event = null;
    try {
        event = await Event().find({ });
        console.log('Event >> ', event);
    }catch(error) {
        console.log('Error >> ', error);
    }

    ctx.body = {
        event: event
    }
}