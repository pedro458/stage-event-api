'use strict';

/**
 * Validation errors
 */
module.exports.EMAIL_CONFLICT = {
  statusCode: 409,
  code: 'EMAIL_CONFLICT',
  message: 'Esse e-mail já está em uso'
}

/**
 * Server Errors
 */
module.exports.INTERNAL_ERROR = {
  statusCode: 500,
  code: 'INTERNAL_ERROR',
  message: 'The server encountered an internal error.'
};

module.exports.UNKNOWN_ERROR = {
  statusCode: 500,
  code: 'UNKNOWN_ERROR',
  message: 'The server encountered an unknown error.'
};
