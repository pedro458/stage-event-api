'use strict';

const Router = require('koa-router');
const { validateRequest } = require('../modules/validator');
const eventsController = require('../controllers/eventController');

const router = Router({
    prefix: '/events'
})

router.post('/approve', eventsController.approveRequest);
router.get('/basicInfo', eventsController.getBasicInfo);

module.exports = router;