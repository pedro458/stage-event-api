'use strict';

const Router = require('koa-router');
const { validateRequest } = require('../modules/validator');
const guestController = require('../controllers/guestController');

const router = Router({
    prefix: '/guest'
})

router.post('/new', guestController.createGuest);

module.exports = router;