'use strict';

const supertest = require('supertest');
const ObjectId = require('mongoose').Types.ObjectId;
const { expectBoomError } = require('../utils');

const App = require('../../app/app');

const {
    EVENT_REQUEST_NOT_FOUND
} = require('../../app/constants/error');

const baseRoute = '/events';
const fakeId = ObjectId();

// Inicializando o server
const server = new App().listen();

describe('Test events controller', () => {
    const request = supertest(server);

    // Testar endpoint de aprovar as requisiçoes de evento
    describe('Test POST /approve/', () => {
        const route = `${ baseRoute }/approve`;

        // 4. Testar lógica do controller
        // 4.1 Testar evento não encontrado
        test('404 event no found', () => {
            return request.post(route)
            .send({ _event_request_id: fakeId })
            .set('Accept', 'application/json')
            .then(expectBoomError(EVENT_REQUEST_NOT_FOUND));
        })
        
    });
})