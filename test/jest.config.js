process.env.NODE_ENV = 'test';
process.env.MONGODB_URI = 'mongodb://localhost:27017/ur_test';

module.exports = {
    setupFilesAfterEnv: ['./jest.setup.js'],
    testEnvironment: 'node',
    verbose: true
}
