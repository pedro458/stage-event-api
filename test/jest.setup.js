const mongoose = require('mongoose');
const { MONGOOSE_OPTS } = require('../app/utils/constants');

beforeAll(async () => {
    await mongoose.connect(process.env.MONGODB_URI, MONGOOSE_OPTS);
});

afterAll(async () => {
    await mongoose.connection.dropDatabase();
    await mongoose.disconnect();
})