const expectBoomError = errorObject => response => {
    expect(response.statusCode).toBe(errorObject.statusCode);
    expect(response.body).toMatchObject({
      statusCode: errorObject.statusCode,
      message: errorObject.message,
    });
  }

module.exports = {
    expectBoomError
}