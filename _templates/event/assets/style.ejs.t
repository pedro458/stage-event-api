---
to: <%= root_path %>/assets/style.css
---
@keyframes aumentar {
	from { width: 0%; }
	to { width: 100%; }
}

@keyframes fadein {
	from { opacity: 0; }
	to { opacity: 1; }
}

body {
	 overflow-x: hidden;
}

#modal-footer {
	padding: 0;
}

#inscreverSBC {
	width: 100%;
	height: 100%;
	padding: 1rem;
	border: 0;
	text-decoration: none;
	color: #fff;
	text-align: center;
	font-size: 1.4em;
}

.desativado {
	background-color: #dee2e6;
	pointer-events: none;
	cursor: default;
}

.ativado {
	background-color: #2777B9;
	pointer-events: all;
	cursor: pointer;
}

.ativado:hover {
	background-color: #1E629A;
	transition: 0.5s;
	-moz-transition: 0.5s;
	-webkit-transition: 0.5s;
}

@media (max-width: 992px) {
	#hospedagem > h2 {
		width: 80%;
		margin-left: 10%;
		margin-right: 10%;
	}
}

@media (max-width: 460px) {
	#apresentacao iframe {
		display: none;
	}
}
.Topo:hover {
	background-color: rgba(200, 80, 0, 1);
	text-decoration: none;
	color: #222;
	transition: 0.3s;
	-webkit-transition: 0.3s;
	-moz-transition: 0.3s;
}

.Topo {
	position: fixed;
  bottom: 2em;
  right: 2em;
  text-decoration: none;
	color: white;
  background-color: rgba(253, 128, 1, 1);
  font-size: 1em;
  padding: 1em;
  display: none;
  border-radius: 4px;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
}

.blind {
	display: none;
}

div {
	font-family: 'Spinnaker', sans-serif;
}

#apresentacao h2,
#hospedagem h2,
#contato h2 {
	font-weight: bold;
}