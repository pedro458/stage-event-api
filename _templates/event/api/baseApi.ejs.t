---
to: <%= root_path %>/api/baseApi.js
---
import Axios from 'axios';

const baseApi = Axios.create({
    // /* URL usada para testes 
    // baseURL: "http://localhost:1337/",
    // */
})

export default baseApi;