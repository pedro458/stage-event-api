---
to: <%= root_path %>/components/index.js
---
import Header from './Header/header';
import Carousel from './Carousel/carousel';
import About from './About/about';
import Schedule from './Schedule/schedule';
import Talks from './Speakers/talks';
import Subscription from './Subscription/subscription';
import Papers from './Papers/papers';
import Lodging from './Lodging/lodging';
import Contact from './Contact/contact';
import Footer from './Footer/footer'

export {
    // Header
    Header,
    // Carousel
    Carousel,
    // About
    About,
    // Schedule
    Schedule,
    // Talks
    Talks,
    // Subscriptions
    Subscription,
    // Papers
    Papers,
    // Lodging
    Lodging,
    // Contact
    Contact,
    // Footer
    Footer
}