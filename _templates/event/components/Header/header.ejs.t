---
to: <%= root_path %>/components/Header/header.js
---
import React from 'react';
import cx from 'classnames';

import NavLinks from './navLinks';

import {
    navbar,
    navbarExpandLg,
    navbarDark,
    darkBg,
    fixedTop,
    navbarBrand,
    navbarToggler,
    navbarTogglerIcon,
    collapse,
    navbarCollapse,
} from '../../assets/jss/globalStyle';

import {
    headerStyle
} from '../../assets/jss/components-jss'

export default (props) => {
    const navbarClasses = cx({
        [navbar]: true,
        [navbarExpandLg]: true,
        [navbarDark]: true,
        [darkBg]: true,
        [fixedTop]: true
    })

    const menuClasses = cx({
        [collapse]: true,
        [navbarCollapse]: true
    })

    return (
        <header>
            <nav className={ navbarClasses }>
                <h1 id="head">
                <a href="http://www.sbc.org.br/" target="_blank" id="main-logo">{ props.data.logo ? <img src={ require('../../assets/imgs/eripi-sbc.png') } alt="ERIPI"/> : props.data.eventName }</a>
                </h1>
                <a className={ navbarBrand } href="#"></a>
                <button className={ navbarToggler } type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation">
                    <span className={ navbarTogglerIcon }></span>
                </button>
                <div className={ menuClasses } id="menu">
                    <NavLinks general={ props.data } />
                </div>
            </nav>
            <style jsx>{ headerStyle }</style>
	</header>
    )
}