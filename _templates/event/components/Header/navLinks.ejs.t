---
to: <%= root_path %>/components/Header/navLinks.js
---
import React from 'react';
import cx from 'classnames';

import {
    navbarNav,
    mtLg0,
    mxAuto,
    navItem,
    navLink,
    btnGroup,
    btn,
    dropdownToggle,
    dropdownMenu,
    dropdownItem
} from '../../assets/jss/globalStyle';

export default (props) => {
    const menuUlClasses = cx({
        [navbarNav]: true,
        [mtLg0]: true,
        [mxAuto]: true
    })

    const aLiClasses = cx({
        [navLink]: true,
        ['link']: true
    })

    const dropdownBtnClasses = cx({
        [btn]: true,
        [dropdownToggle]: true
    })

    const dropdownAClasses = cx({
        [dropdownItem]: true,
        ['link']: true
    })


    return (
        <ul className={ menuUlClasses }>
            {
                props.general.sections.map(section => ( <li className={ navItem }><a className={ aLiClasses } href={ `#${ section }` }>{ section }</a></li> ))
            }
        </ul>
    )
}