---
to: <%= root_path %>/components/Footer/footer.js
---
import React from 'react';
import moment from 'moment';

import {
    footerStyle
} from '../../assets/jss/components-jss'

export default (props) => {
    return (
        <footer>
			&copy; { moment().year() } { props.data.eventName }. Todos os direitos reservados | Desenvolvido por:
			<a className="dev" href="http://factos.ufpi.br" target="_blank"> FACTOS</a>
			<a href="#" class="Topo"><i class="fas fa-arrow-up"></i></a>
            <style jsx>{ footerStyle }</style>
		</footer>
    )
}