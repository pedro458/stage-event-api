---
to: <%= root_path %>/components/Carousel/carousel.js
---

import React from 'react';
import cx from 'classnames'

import {
    carousel,
    slide,
    carouselIndicators,
    active,
    carouselInner,
    carouselItem,
    imgText,
    carouselControlPrev,
    carouselControlPrevIcon,
    srOnly,
    carouselControlNext,
    carouselControlNextIcon,
    containerFluid
} from '../../assets/jss/globalStyle';

import {
    carouselStyle
} from '../../assets/jss/components-jss';

export default (props) => {
    const myCarouselClasses = cx({
        [carousel]: true,
        [slide]: true
    })

    const carouselItemClasses = cx({
        [carouselItem]: true,
        [active]: true
    })

    const injectImgText = cx({
        [imgText]: true,
        ['img-text']: true
    })

    return (
        <section id="intro">
        <div id="myCarousel" className={ myCarouselClasses } data-ride="carousel" data-interval="5000">
            <ol className={ carouselIndicators }>
                <li data-target="#myCarousel" data-slide-to="0" className={ active }></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>
            
            <div className={ carouselInner }>
                <div className={ carouselItemClasses }>
                    <img src={ require('../../assets/imgs/intro/fundo1.jpg') } alt="First slide" />
                    <img className={ injectImgText } src={ require('../../assets/imgs/intro/slide1.png') } alt="First slide" id="overlay"/>
                </div>
                <div className={ carouselItem }>
                    <img src={ require('../../assets/imgs/intro/fundo2.jpg') } alt="First slide" />
                    <img className={ injectImgText } src={ require('../../assets/imgs/intro/slide2.png') } alt="First slide" />
                </div> 
            </div>

            <a className={ carouselControlPrev } href="#myCarousel" role="button" data-slide="prev">
                <span className={ carouselControlPrevIcon } aria-hidden="true"></span>
                <span className={ srOnly }>Previous</span>
            </a>
            <a className={ carouselControlNext } href="#myCarousel" role="button" data-slide="next">
                <span className={ carouselControlNextIcon } aria-hidden="true"></span>
                <span className={ srOnly }>Next</span>
            </a>
        </div>
        <style jsx>{ carouselStyle }</style>
    </section>
    )
}