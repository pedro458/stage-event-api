---
to: <%= root_path %>/components/Schedule/days.js
---
import React from 'react';
import cx from 'classnames';

import {
    active,
    nav,
    navTabs
} from '../../assets/jss/globalStyle';

import {
    daysStyle
} from '../../assets/jss/components-jss'

export default (props) => {
    const navClasses = cx({
        [nav]: true,
        [navTabs]: true
    })

    const diaActiveClasses = cx({
        ['dia']: true,
        [active]: true,
        ['show']: true
    })

    const diaDeactiveClasses = cx({
        ['dia']: true,
        [active]: false,
        ['show']: false
    })

    const calendarIconFa = cx({
        ['fa']: true,
        ['fa-calendar-alt']: true
    })

    let days = props.activities.map(act => act.day)
    days = new Set(days);
    days = Array.from(days);

    return(
        <ul className={ navClasses } role="tablist">
            {
                days.map((day, index) => (
                    <li role="presentation" className={ props.activated[index] ? active : '' } onClick={ () => props.handleOnChangeDay(index) }>
                        <a className={ props.activated[index] ? diaActiveClasses : diaDeactiveClasses } aria-controls="segunda" role="tab" data-toggle="tab">
                            <i className={ calendarIconFa }></i>
                            &nbsp; { day }
                        </a>
                    </li>
                ))
            }
            <style jsx>{ daysStyle }</style>
        </ul>
    )
}