---
to: <%= root_path %>/components/Schedule/tabPanel.js
---
import React from 'react';
import cx from 'classnames';

import Activity from './activity';

import {
    active,
    tabPane,
    table,
    tableStriped
} from '../../assets/jss/globalStyle'

export default (props) => {
    const tabActiveClasses = cx({
        [active]: true,
        [tabPane]: true
    })

    const tabDeactiveClasses = cx({
        [active]: false,
        [tabPane]: true
    })

    const tableStripedClasses = cx({
        [table]: true,
        [tableStriped]: true
    })

    return (
        <div role="tabpanel" className={ props.activated ? tabActiveClasses : tabDeactiveClasses } id="segunda">
            <table className={ tableStripedClasses }>
                <tbody>
                    {
                        props.activities.map(act => (<Activity data={ act }/>))
                    }
                </tbody>
            </table>
        </div>
    )
}