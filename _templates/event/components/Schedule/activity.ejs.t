---
to: <%= root_path %>/components/Schedule/activity.js
---
import React from 'react';
import cx from 'classnames';

import {
    tableStyle
} from '../../assets/jss/components-jss';

export default (props) => {
    const clockIconFa = cx({
        ['far']: true,
        ['fa-clock']: true
    })

    const mapMarkerIconFa = cx({
        ['fas']: true,
        ['fa-map-marker-alt']: true
    })

    return (
        <tr>
            <td><i className={ clockIconFa }></i>&nbsp; { `${ props.data.dataIni } - ${ props.data.dataFim }` }</td>
            <td>{ props.data.desc }</td>
            {/* <td><i className={ mapMarkerIconFa }></i>&nbsp; CTT: Auditório</td> */}
            <style jsx>{ tableStyle }</style>
        </tr>
    )
}