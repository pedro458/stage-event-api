---
to: <%= root_path %>/components/Schedule/schedule.js
---
import React, { Component } from 'react';
import cx from 'classnames';

import Days from './days';
import TabPanel from './tabPanel';

import {
    containerFluid,
    row,
    colMd12,
    active,
    tabContent,
    tabPane,
    table,
    tableStriped
} from '../../assets/jss/globalStyle';

import {
    scheduleStyle
}  from '../../assets/jss/components-jss';

export default class Schedule extends Component {
	constructor(props) {
		super(props);
		this.state = {
			activatedFlags: [true, false]
		}
		this.handleOnChangeDay = this.handleOnChangeDay.bind(this);
	}

	handleOnChangeDay(index) {
		let activatedFlagsCopy = this.state.activatedFlags;
		activatedFlagsCopy = [false, false];
		activatedFlagsCopy[index] = true;
		this.setState({
			activatedFlags: activatedFlagsCopy
		})
	}

	render() {
		// Separating events by day
		let days = this.props.data.activities.map(act => act.day)
		days = new Set(days);
		days = Array.from(days);
		let events_01 = this.props.data.activities.filter((act) => { return act.day === days[0] })
		let events_02 = this.props.data.activities.filter((act) => { return act.day === days[1] })

		return (
			<section id="programacao">
				<h2 data-aos="fade-up">Programação</h2>
				<div data-aos="zoom-in" className={ containerFluid }>
					<div className={ row }>
						<div className={ colMd12 }>
							<Days activities={ this.props.data.activities } activated={ this.state.activatedFlags } handleOnChangeDay={ this.handleOnChangeDay }/>
							<div className={ tabContent }>	
								<TabPanel activities={ events_01 } activated={ this.state.activatedFlags[0] }/>
								<TabPanel activities={ events_02 } activated={ this.state.activatedFlags[1] }/>
							</div>
						</div>
					</div>
				</div>
				<style jsx>{ scheduleStyle }</style>
			</section>
		)
	}	
}