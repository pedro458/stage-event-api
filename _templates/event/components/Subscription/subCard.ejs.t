---
to: <%= root_path %>/components/Subscription/subCard.js
---
import React from 'react';
import cx from 'classnames';

import {
    table,
    tableStriped,
    colMd4
} from '../../assets/jss/globalStyle';

import {
    subCardStyle
} from '../../assets/jss/components-jss'

export default (props) => {
    const tableStripedClasses = cx({
		[table]: true,
		[tableStriped]: true
	})

	const injectColMD04Class = cx({
		[colMd4]: true,
		['col-md-4']: true
	})

    let types = props.data.prices.map(price => price.type)
    types = new Set(types);
    types = Array.from(types);

    return (
        <div className={ injectColMD04Class } data-aos="fade-up" data-aos-delay="100">
            <h3>Estudante de Graduação</h3>
            <table className={ tableStripedClasses }>
                <tbody>
                    <tr>
                        <th>Datas</th>
                        {
                            types.map(type => <th>{ type }</th>)
                        }
                    </tr>
                    <tr>
                        <td>{` De ${ props.data.dataIni } até ${ props.data.dataFim } `}</td>
                        {
                            props.data.prices.map(price => (<td>{ price.val }</td>))
                        }
                    </tr>
                </tbody>
            </table>
            {/* <a href={ props.data.link } target='_blank'>Inscrever-se</a> */}
            <a href={ props.link } target='_blank'>Inscrever-se</a>
            <style jsx>{ subCardStyle }</style>
        </div>	
    )
}