---
to: <%= root_path %>/components/Subscription/subscription.js
---
import React from 'react';
import cx from 'classnames';

import SubCard from './subCard';

import {
	containerFluid,
	row,
	colMd4,
	table,
	tableStriped
} from '../../assets/jss/globalStyle';

import {
	subscriptionStyle
} from '../../assets/jss/components-jss'

export default (props) => {
	const tableStripedClasses = cx({
		[table]: true,
		[tableStriped]: true
	})

	const injectContainerClass = cx({
		[containerFluid]: true,
		['container-fluid']: true
	})

	const injectColMD04Class = cx({
		[colMd4]: true,
		['col-md-4']: true
	})

    return (
        <section id="inscricoes">
			<h2 data-aos="fade-left">{ props.data.sectionName }</h2>
			<div className={ injectContainerClass }>
				<div className={ row }>
					{
						props.data.subscriptions.map(sub => (<SubCard data={ sub } link={ props.data.link }/>))
					}
				</div>
			</div>
			<style jsx>{ subscriptionStyle }</style>
		</section>
    )
}