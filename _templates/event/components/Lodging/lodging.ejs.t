---
to: <%= root_path %>/components/Header/lodging.js
---
import React from 'react';
import cx from 'classnames';

import {
    containerFluid,
    row,
    colMd6,
    card,
    colLg6,
    colMd4,
    colSm5,
    colSm1,
    colMd2,
    colMd5,
    colSm6
} from '../../assets/jss/globalStyle';

import {
    lodgingStyle
} from '../../assets/jss/components-jss';

export default (props) => {
    const injectCardClass = cx({
        [card]: true,
        ['card']: true
    })

    const colLg6CartaoClasses = cx({
        [colLg6]: true,
        ['cartao']: true
    })

    const colCartaoClasses = cx({
        [colMd4]: true,
        [colSm5]: true,
        ['cartao']: true
    })

    const colMd2ColSm1Classes = cx({
        [colMd2]: true,
        [colSm1]: true
    })

    const colMd5ColSm6CartaoClasses = cx({
        [colMd5]: true,
        [colSm6]: true,
        ['cartao']: true
    })

    return(
        <section id="hospedagem">
			<h2 data-aos="zoom-in-up">Hospedagem</h2>
			<div className={ containerFluid }>
				<div className={ row }>
					<div className={ colMd6 }>
						<div className={ injectCardClass } data-aos="fade-up-right">
							<h3><i className="fas fa-home"></i> Bristol Gran Hotel Arrey</h3>
							<p><i className="fas fa-map-marker"></i> &nbsp; <b>Endereço:</b> R. Jaime da Silveira, 433 - São Cristovão, Teresina - PI, 64056-330</p>
							<p><i className="fas fa-phone"></i> &nbsp; <b>Telefone:</b> (86) 3214-9292</p>
							<p><i className="fas fa-mobile-alt"></i> &nbsp; <b>Celular:</b> (86) 99497-3342</p>
							<p><i className="fas fa-road"></i> &nbsp; <b>Tempo e Distância para o evento:</b> 16 min (6,4 km)</p>
							<p><i className="fas fa-map"></i> &nbsp; <b>Mapa:</b> <a href="https://goo.gl/maps/MfpJEKpa8FJ2" target="_blank">https://goo.gl/maps/MfpJEKpa8FJ2</a></p>
						</div>
					</div>
					<div className={ colMd6 }>
						<div className={ injectCardClass } data-aos="fade-up-left">
							<div className="em-breve" style={ { backgroundColor: 'transparent' } }>
								<i className="far fa-clock"></i>&nbsp; Em breve divulgaremos mais opções.
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="realizacao">
				<div className={ containerFluid }>
					<div className={ row }>
						<div className={ colLg6CartaoClasses } data-aos="fade-up">
							<h2 data-aos="fade-down">Patrocínio</h2>
							<div className="imagem-grupo">
								<a className="imagem" href="https://infoway-br.com/en/" target="_blank">
									<img src={ require("../../assets/imgs/patrocinadores/logo-infoway.png") }/>
								</a>
								<a className="imagem" href="https://tron-edu.com/" target="_blank">
									<img src={ require("../../assets/imgs/patrocinadores/logo-TRON.png") }/>
								</a>
								<a className="imagem" href="http://www.likemagic.com.br/" target="_blank">
									<img src={ require("../../assets/imgs/patrocinadores/logo-likemagic.png") }/>
								</a>
							</div>
						</div>
					</div>

					<div className={ row }>
						
						<div className={ colCartaoClasses } data-aos="fade-up">
							<h2 data-aos="fade-down">Realização</h2>
							<a className="imagem-centro" href="http://www.sbc.org.br/" target="_blank">
								<img src={ require("../../assets/imgs/realizacao/logo-sbc.png") }/>
							</a>
						</div>

						<div className={ colMd2ColSm1Classes }></div>
						
						<div className={ colMd5ColSm6CartaoClasses } data-aos="fade-up">
							<h2 data-aos="fade-down">Execução</h2>
							<div className="imagem-grupo">
								<a className="imagem" href="http://ufpi.br" target="_blank">
									<img src={ require("../../assets/imgs/realizacao/logo-ufpi.png") }/>
								</a>
								<a className="imagem" href="http://www.uespi.br/site/" target="_blank">
									<img src={ require("../../assets/imgs/realizacao/logo-uespi.png") }/>
								</a>
								<a className="imagem" href="http://www.ifpi.edu.br/" target="_blank">
									<img src={ require("../../assets/imgs/realizacao/logo-ifpi.png") }/>
								</a>
							</div>
						</div>
					</div>

					<div className={ row }>
						<div className={ colLg6CartaoClasses } data-aos="fade-up">
							<h2 data-aos="fade-down">Apoio</h2>
							<div className="imagem-grupo">
								<a className="imagem" href="http://www.sigaa.ufpi.br/sigaa/public/programa/portal.jsf?id=615" target="_blank">
									<img src={ require("../../assets/imgs/realizacao/logo-ppgcc.png") }/>
								</a>
								<a className="imagem" href="http://factos.ufpi.br/" target="_blank">
									<img src={ require("../../assets/imgs/realizacao/logo-factos.svg") }/>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
            <style jsx>{ lodgingStyle }</style>
		</section>
    )
}