---
to: <%= root_path %>/components/About/comission.js
---

import React from 'react';

import {
    comissionStyle
} from '../../assets/jss/components-jss'

export default (props) => {
    return (
        <div>
            <h3>{ props.name }</h3>
            <ul>
                {
                    props.peoples.map(p => (
                        <li key={ p }>{ p }</li>
                    ))
                }
            </ul>
            <style jsx>{ comissionStyle }</style>
        </div>
    )
}