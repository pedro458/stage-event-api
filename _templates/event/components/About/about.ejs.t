---
to: <%= root_path %>/components/About/about.js
---

import React from 'react';
import cx from 'classnames';

import Comission from './comission';

import {
    containerFluid,
    row,
    colMd6,
    colLg7,
    colLg5
} from '../../assets/jss/globalStyle';

import {
    aboutStyle
} from '../../assets/jss/components-jss';

export default (props) => {
    const sobreClasses = cx({
        [colMd6]: true,
        ['sobre']: true
	})

    return (
        <section id="apresentacao">
			<h2 style={ { display: 'block', margin: 'auto auto' } }>Realizado por:</h2>
			{
				props.data.realization ?
				(
					<a href="http://www.sbc.org.br/" target="_blank">
						<img src={ require("../../assets/imgs/realizacao/logo-sbc.png") } alt="ERIPI" style={{ display: 'block', margin: '40px auto' }} />
					</a>
				)
				:
				( <h3 id="placeholder-realization">{ props.general.realization }</h3> )
			}
			<h2 data-aos="fade-up">{ props.data.sectionName }</h2>
			<div className={ containerFluid }>
				<div className={ row }>
					{/* <div className={ colMd6 }>
						<figure data-aos="fade-right">
							<img src={ require("../../assets/imgs/logo-eripi.png") }/>
						</figure>
					</div> */}
					<div className={ sobreClasses } data-aos="fade-left">
						<p>{ props.data.info }</p>
					</div>
				</div>
			</div>
			<h2 id="comissao" data-aos="flip-right">Comissões</h2>
			<div className={ containerFluid } data-aos="fade-up">
				<div className={ row }>
					<div className={ colLg7 }>
						{ 
							props.data.comissions.map(comission => (
								<Comission name={ comission.name } peoples={ comission.peoples }/>
							))
						}
					</div>
					{/* <div className={ colLg5 } data="facebook-feed">
						<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Feripioficial%2F&tabs=timeline&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
					</div> */}
				</div>
			</div>
            <style jsx>{ aboutStyle }</style>
		</section>
    )
}