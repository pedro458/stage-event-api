---
to: <%= root_path %>/components/Papers/papers.js
---
import React from 'react';
import cx from 'classnames'

import {
    containerFluid,
    row,
    colMd4,
    colMd8,
    nav,
    navTabs,
    flexColumn,
    active,
    navLink,
    tabContent,
    tabPane,
    fade
} from '../../assets/jss/globalStyle';

import {
    paperStyle
} from '../../assets/jss/components-jss'

export default (props) => {
    const injectColMd4 = cx({
        [colMd4]: true,
        ['col-md-4']: true
    })

    const injectNavFlex = cx({
        [nav]: true,
        [flexColumn]: true,
        [navTabs]: true,
        ['nav']: true,
        ['flex-column']: true,
        ['nav-tabs']: true
    })

    const injectNavLinkActive = cx({
        [navLink]: true,
        [active]: true,
        ['active']: true,
        ['nav-link']: true
    })

    const arrowCircleFaIcon = cx({
        ['far']: true,
        ['fa-arrow-alt-circle-right']: true
    })

    const injectNavLink = cx({
        [navLink]: true,
        ['nav-link']: true
    })

    const injectColMd8 = cx({
        [colMd8]: true,
        ['col-md-8']: true
    })

    const injectTabContent = cx({
        [tabContent]: true,
        ['tab-content']: true
    })

    const tabPaneActiveFadeClasses = cx({
        [tabPane]: true,
        [fade]: true,
        [active]: true
    })

    const tabPaneFadeClasses = cx({
        [tabPane]: true,
        [fade]: true,
    })

    return (
        <section id="artigos" className="sub-trabalhos">
            <h2>Submissão de Artigos</h2>
            <div className={ containerFluid }>
                <div className={ row }>
                    <div className={ injectColMd4 }>
                        <div className={ injectNavFlex } id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a className={ injectNavLinkActive } id="artigos_1_tab" data-toggle="tab" href="#artigos_1" role="tab" aria-controls="artigos_1" aria-selected="true">
                                <i className={ arrowCircleFaIcon }></i> &nbsp; Chamada de Artigos
                            </a>
                            <a className={ injectNavLink } id="artigos_2_tab" data-toggle="tab" href="#artigos_2" role="tab" aria-controls="artigos_2" aria-selected="false">
                                <i className={ arrowCircleFaIcon }></i> &nbsp; Regras para Submissão
                            </a>
                            <a className={ injectNavLink } id="artigos_3_tab" data-toggle="tab" href="#artigos_3" role="tab" aria-controls="artigos_3" aria-selected="false">
                                <i className={ arrowCircleFaIcon }></i> &nbsp; Premiação
                            </a>
                            <a className={ injectNavLink } id="artigos_4_tab" data-toggle="tab" href="#artigos_4" role="tab" aria-controls="artigos_4" aria-selected="false">
                                <i className={ arrowCircleFaIcon }></i> &nbsp; Temas de Interesse
                            </a>
                            <a className={ injectNavLink } id="artigos_5_tab" data-toggle="tab" href="#artigos_5" role="tab" aria-controls="artigos_5" aria-selected="false">
                                <i className={ arrowCircleFaIcon }></i> &nbsp; Datas Importantes
                            </a>
                        </div>
                    </div>
                    <div className={ colMd8 }>
                        <div className={ injectTabContent } id="v-pills-tabContent">
                            <div className={ tabPaneActiveFadeClasses } id="artigos_1" role="tabpanel" aria-labelledby="artigos_1_tabs">
                                <h3>Chamada de Artigos</h3>
                                <p>O Comitê de Organização da ERIPI 2018 (Escola Regional de Informática do Piauí) convida professores, alunos e profissionais de computação e áreas afins a submeter artigos científicos, descrevendo resultados de trabalhos originais de pesquisa ou de aplicação tecnológica. Cada artigo será submetido à avaliação cega por pares (double-blind reviewing) e julgado segundo sua relevância técnico-científica, adequação ao evento, estrutura, qualidade do texto, resultados apresentados, dentre outros aspectos.</p>
                                <p>Os artigos devem ser submetidos através da plataforma JEMS (<a href="https://submissoes.sbc.org.br/home.cgi?c=3013" target="_blank">acesso aqui</a>).</p>
                                <p>Todos os artigos devem estar no formato de arquivo PDF e deverão ser submetidos em inglês ou português, com no máximo 6 páginas, incluindo todas as figuras e referências. Os artigos devem seguir obrigatoriamente o formato definido pela SBC - Sociedade Brasileira de Computação (<a href="http://sbc.org.br/documentos-da-sbc/summary/169-templates-para-artigos-e-capitulos-de-livros/878-modelosparapublicaodeartigos" target="_blank">disponível aqui</a>) e não conter informações que possam identificar dos autores.</p>
                                
                                <p>Artigos fora desse padrão serão sumariamente rejeitados. A aceitação de um artigo implica que pelo menos um dos seus autores irá se inscrever no evento para apresentá-lo em sessão técnica oral ou na forma de pôster.</p>
                            </div>
                            <div className={ tabPaneFadeClasses } id="artigos_2" role="tabpanel" aria-labelledby="artigos_2_tabs">
                                <h3>Regras para Submissão de Artigos</h3>
                                <ul>
                                    <li>Todos os artigos devem estar no formato Adobe PDF;</li>
                                    <li>Deverão ser escritos em inglês ou português;</li>
                                    <li>Os artigos não poderão ultrapassar 6 páginas, incluindo tabelas, figuras e referências.</li>
                                    <li>Na página de rosto do artigo não deverá constar nenhum tipo de identificação dos autores. Os artigos que possuírem identificação serão imediatamente rejeitados.</li>
                                    <li>Os autores também podem omitir, se desejarem, informações no texto que permitam inferir a autoria do artigo (As informações de autoria deverão ser fornecidas exclusivamente na submissão pelo sistema JEMS).</li>
                                    <li>Na versão final do artigo, caso aceito, os dados dos autores e filiações deverão ser incluídos.</li>
                                    <li>Recomenda-se evitar a menção direta a nomes pessoais (professores, alunos, escolas, municípios, etc.) trocando-os por identificadores genéricos (Escola A, Aluno 1, etc.).</li>
                                    <li>Todo e qualquer artigo para o ERIPI 2018 deverá ser submetida apenas em formato eletrônico, por meio do sistema JEMS.
                                    A aceitação de um artigo implica que pelo menos um dos seus autores irá se inscrever no evento para apresentá-lo em sessão técnica oral.</li>
                                </ul>
                            </div>
                            <div className={ tabPaneFadeClasses } id="artigos_3" role="tabpanel" aria-labelledby="artigos_3_tabs">
                                <h3>Premiação</h3>
                                <p>O Comitê Técnico anunciará durante o evento os melhores artigos da ERIPI 2018. Os autores desses artigos garantirão também a sua publicação em edições especiais das Revistas:</p>
                                <ul>
                                    <li>Revista de Sistemas e Computação - <a href="http://www.revistas.unifacs.br/index.php/rsc/index" target="_blank">RSC</a></li>
                                    <li>Revista Learning && Nonlinear Models - <a href="http://abricom.org.br/lnlm/" target="_blank">L&&NLM</a></li>
                                </ul>
                            </div>
                            <div className={ tabPaneFadeClasses } id="artigos_4" role="tabpanel" aria-labelledby="artigos_4_tabs">
                                <h3>Temas de Interesse</h3>
                                <p>A lista não-exaustiva de tópicos de interesse inclui:</p>
                                <ul>
                                    <li>Arquitetura de Computadores</li>
                                    <li>Computação Gráfica</li>
                                    <li>Concepção de Circuitos, Sistemas Integrados e Sistemas Embarcados</li>
                                    <li>Engenharia de Software</li>
                                    <li>Gestão de Tecnologia da Informação</li>
                                    <li>Inclusão Digital</li>
                                    <li>Informática Industrial</li>
                                    <li>Informática Médica</li>
                                    <li>Informática na Educação</li>
                                    <li>Inteligência Artificial</li>
                                    <li>Linguagens de Programação</li>
                                    <li>Processamento de Imagens</li>
                                    <li>Redes de Computadores</li>
                                    <li>Sistemas Distribuídos</li>
                                    <li>Sistemas Hipermídia, Multimídia e Web</li>
                                    <li>Sistemas Operacionais</li>
                                    <li>TV Digital</li>
                                </ul>
                            </div>
                            <div className={ tabPaneFadeClasses } id="artigos_5" role="tabpanel" aria-labelledby="artigos_5_tabs">
                                <h3>Datas Importantes</h3>
                                <ul>
                                    <li>Submissão de artigos: 16 de março de 2018</li>
                                    <li>Notificação de aceitação: 27 de abril de 2018</li>
                                    <li>Envio da versão Final: 28 de abril de 2018</li>
                                </ul>
                                <h3>Contato</h3>
                                <p>Prof. Dr. Rodrigo de Melo Souza Veras: <a href="mailto:rveras@ufpi.edu.br">rveras@ufpi.edu.br</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <style jsx>{ paperStyle }</style>
        </section>
    )
}