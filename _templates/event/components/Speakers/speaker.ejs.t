---
to: <%= root_path %>/components/Speakers/speaker.js
---
import React from 'react';
import cx from 'classnames';

import {
    containerFluid,
    row,
    colMd6,
    colMd12,
    legenda,
    card
} from '../../assets/jss/globalStyle';

import {
    speakerStyle
} from '../../assets/jss/components-jss';

export default (props) => {
    const cardPhantomClasses = cx({
        [card]: true,
        ['card']: true
    })

    return (
        <div className={ colMd6 }>
            <div data-aos="fade-right" className={ cardPhantomClasses }>
                <figure>
                    <div className={ containerFluid }>
                        <div className={ row }>
                            {/* <div className={ colMd6 }>
                                <a href="http://lattes.cnpq.br/5123602572479168" target="_blank">
                                    { props.photo ? <img src={ require("../../assets/imgs/palestrantes/p1.jpg") } alt="Alba Cristina Magalhães Alves de Melo" title="Alba Cristina Magalhães Alves de Melo" /> : '' }
                                </a>
                            </div> */}
                            <div className={ colMd12 }>
                                <div className={ legenda }>
                                    <figcaption>{ props.name }</figcaption>
                                    <label>{ props.desc }</label>
                                    { 
                                        props.talkName.length > 0 ? <figcaption>Palestra: <em>{ props.talkName }</em></figcaption> : ''
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </figure>
            </div>
            <style jsx>{ speakerStyle }</style>
        </div>
    )
}