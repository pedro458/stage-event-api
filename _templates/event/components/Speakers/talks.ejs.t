---
to: <%= root_path %>/components/Speakers/talks.js
---
import React from 'react';

import Speaker from './speaker';

import {
    containerFluid,
    row,
    colMd6,
    card,
    legenda
} from '../../assets/jss/globalStyle';

import {
    talkStyle
} from '../../assets/jss/components-jss';

export default (props) => {
    return (
        <section id="palestrantes">
			<h2 data-aos="fade-down">{ props.data.sectionName }</h2>
			<div className={ containerFluid }>
				<div className={ row }>
					{
						props.data.talks.map(talk => {
							return (<Speaker name={ talk.speaker.name } desc={ talk.speaker.desc } talkName={ talk.talkName }/>)
						})
					}
				</div>
			</div>
            <style jsx>{ talkStyle }</style>
		</section>
    )
}