---
to: <%= root_path %>/components/Contact/contact.js
---
import React from 'react';
import cx from 'classnames';

import {
    containerFluid,
    row,
    colMd6,
    info,
    formInput
} from '../../assets/jss/globalStyle';

import {
    contactStyle
} from '../../assets/jss/components-jss';

export default (props) => {
    const injectInfoClass = cx({
        [info]: true,
        ['info']: true
    })

    const injectFormInputClass = cx({
        [formInput]: true,
        ['form-input']: true
    })

    return (
        <section id="contato">
			<h2 data-aos="fade-right">Contato<span></span></h2>
			<div className={ containerFluid }>
				<div className={ row }>
					<div className={ colMd6 } data-aos="fade-right">
						<div className={ injectInfoClass }>
							<h3>{ props.general.eventName }</h3>	
							{/* <p><i className="fas fa-map-marker"></i> &nbsp; CTT - Colégio Técnico de Teresina</p> */}
							<p><i className="far fa-calendar-alt"></i> &nbsp; Início: { props.general.dataIni }</p>
							<p><i className="fas fa-envelope"></i> &nbsp; E-mail: { props.general.contact.email }</p>
							<p><i className="fas fa-phone"></i> &nbsp; Telefone: { props.general.contact.phone }</p>
						</div>
						{/* <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3974.34399076162!2d-42.784222100222735!3d-5.047861811008157!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x78e397c00000001%3A0xcde32e688ebf8fca!2sCol%C3%A9gio+T%C3%A9cnico+de+Teresina+-+CTT!5e0!3m2!1spt-BR!2sbr!4v1533158609044"></iframe> */}
					</div>
					{/* <div className={ colMd6 } data-aos="fade-left">
						<div className={ injectInfoClass }>
							<h3>Fale Conosco</h3>
						</div>
						<form id="form-contato" method="post" action="mail.php">
							<input className={ injectFormInputClass } type="text" name="nome" placeholder="Nome" required />
							<input className={ injectFormInputClass }  type="email" name="email" placeholder="Email" required />
							<input className={ injectFormInputClass }  type="text" name="assunto" placeholder="Assunto" required />
							<textarea className={ injectFormInputClass }  name="mensagem" placeholder="Mensagem" rows="5" required></textarea>
							<input type="submit" name="enviar" value="Enviar" />
							<div id="resposta"></div>
						</form>
					</div> */}
				</div>
			</div>
            <style jsx>{ contactStyle }</style>
		</section>
    )
}