---
to: <%= root_path %>/pages/index.js
---

import React, { Component } from 'react';

import {
    Header,
    Carousel,
    About,
    Schedule,
    Talks,
    Subscription,
    Papers,
    Lodging,
    Contact,
    Footer
} from '../components'

import {
    indexStyle
} from '../assets/jss/components-jss';

export default class Index extends Component {

    static async getInitialProps(){
        try {
            let general = require('../API/Data/general.json');
            let about = require('../API/Data/about.json');
            let talks = require('../API/Data/talks.json');
            let schedule = require('../API/Data/schedule.json');
            let subscriptions = require('../API/Data/subscription.json');
            return { about, talks, schedule, general, subscriptions }
        } catch(e) {
            console.error(e);
        }
    }

    render(){
        return(
            <div>
                <Header data={ this.props.general }/>
                {/* <Carousel /> */}
                <div className="apresentacao-borda">
			        <div className="line"></div>
		        </div>
                <About data={ this.props.about } general={ this.props.general }/>
                <div className="apresentacao-borda">
			        <div className="line-reverse"></div>
		        </div>
                <Schedule data={ this.props.schedule }/>
                <Talks data={ this.props.talks }/>
                <Subscription data={ this.props.subscriptions }/>
                {/* <Papers /> */}
                {/* <Lodging /> */}
                <Contact general={ this.props.general }/>
                <Footer data={ this.props.general }/>
                <style jsx>{ indexStyle }</style>
            </div>
        )
    }
}