---
to: <%= root_path %>/package.json
---

{
  "name": "<%= h.changeCase.paramCase(event_name) %>",
  "version": "1.0.0",
  "description": "App React gerado automaticamente para <%= event_name %> ",
  "main": "server.js",
  "author": "UFPI - Pedro Henrique",
  "license": "MIT",
  "private": true,
  "scripts": {
    "dev": "REACT_SPINKIT_NO_STYLES=true node server.js -p $PORT $API_URL",
    "build": "next build",
    "start": "NODE_ENV=production node server.js -p $PORT",
    "export": "next export" 
  },
  "babel": {
    "presets": [
      "next/babel"
    ],
    "plugins": [
      [
        "styled-jsx/babel",
        {
          "plugins": [
            "styled-jsx-plugin-postcss"
          ]
        }
      ]
    ]
  },
  "dependencies": {
    "@zeit/next-css": "^0.2.0",
    "@zeit/next-sass": "^0.2.0",
    "aos": "^2.3.3",
    "axios": "^0.18.0",
    "better-react-spinkit": "^2.0.4",
    "bulma": "^0.7.1",
    "classnames": "^2.2.6",
    "cross-env": "^5.2.0",
    "express": "^4.16.3",
    "moment": "^2.22.2",
    "next": "^6.1.1",
    "next-compose-plugins": "^2.1.1",
    "next-optimized-images": "^1.4.1",
    "next-routes": "^1.4.2",
    "node-sass": "^4.9.2",
    "numeral": "^2.0.6",
    "postcss-cssnext": "^3.1.0",
    "postcss-import": "^11.1.0",
    "react": "^16.4.2",
    "react-dom": "^16.4.2",
    "react-images": "^0.5.19",
    "react-markdown": "^3.3.4",
    "react-photo-gallery": "^6.0.29",
    "react-share": "^2.2.0",
    "styled-components": "^3.3.3",
    "styled-jsx-plugin-postcss": "^0.1.3"
  }
}
