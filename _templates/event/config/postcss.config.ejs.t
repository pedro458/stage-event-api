---
to: <%= root_path %>/postcss.config.js
---

module.exports = () => ({
    plugins: [
        require('postcss-import')({
            path: ['./']
        }),
        require('postcss-cssnext')({})
    ]
})