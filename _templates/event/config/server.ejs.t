---
to: <%= root_path %>/server.js
---

const express = require('express');
const next = require('next');
const routes = require('./routes');

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler();
const handler = routes.getRequestHandler(app);

app.prepare()
.then(() => {
  // const server = express()
  // server.get('/sobre', (req, res) => {
  //   const actualPage = '/sobre';
  //   app.render(req, res, actualPage);
  // })

  // server.get('/noticia/:id', (req, res) => {
  //   const actualPage = '/noticia';
  //   const queryParams = { _id: req.params.id };
  //   app.render(req, res, actualPage, queryParams);
  // })

  // server.get('*', (req, res) => {
  //   return handle(req, res)
  // })
  const PORT = process.env.PORT || 5000;
  express().use(handler).listen(PORT, (err) => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${ PORT }`)
  })
})
.catch((ex) => {
  console.error(ex.stack)
  process.exit(1)
})