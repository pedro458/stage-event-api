---
to: <%= root_path %>/routes.js
---

const routes = require('next-routes');

module.exports = routes()
.add('index', '/', 'index')