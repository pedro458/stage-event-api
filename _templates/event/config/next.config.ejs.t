---
to: <%= root_path %>/next.config.js
---

const withPlugins = require('next-compose-plugins');
const withCss = require('@zeit/next-css');
const withSass = require('@zeit/next-sass');
const withOptimizedImages = require('next-optimized-images');

const isProd = (process.env.NODE_ENV || 'production') === 'production';

const nextConfig = {
    exportPathMap: () => ({
        '/': { page: '/' },
    }),
}

module.exports = withPlugins([
    [withCss, {
        cssModules: true
    }],
    [withOptimizedImages, {
        
    }],
    [withSass, {
        cssModules: true
    }]
], nextConfig)